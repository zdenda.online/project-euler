package cz.dix.pe.problems;

import cz.dix.pe.tools.StringUtil;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Using names.txt (right click and 'Save Link/Target As...'),
 * a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order.
 * Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list
 * to obtain a name score.
 * <p/>
 * For example, when the list is sorted into alphabetical order, COLIN,
 * which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list.
 * So, COLIN would obtain a score of 938 × 53 = 49714.
 * <p/>
 * What is the total of all the name scores in the file?
 */
public class Problem22 implements Problem {

    @Override
    public Object solve() {
        String inputFilePath = "src/main/resources/names.txt";

        List<String> names = loadNames(inputFilePath);
        long scoresSum = 0;
        for (int pos = 1; pos <= names.size(); pos++) {
            String name = names.get(pos - 1);
            scoresSum += (StringUtil.getAlphabeticalValue(name) * pos);
        }
        return scoresSum;
    }

    private List<String> loadNames(String filePath) {
        String contents;
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(filePath));
            contents = Charset.forName("UTF-8").decode(ByteBuffer.wrap(encoded)).toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String[] names = contents.split(",");
        for (int i = 0; i < names.length; i++) {
            names[i] = names[i].replaceAll("\"", "");
        }
        List<String> namesList = Arrays.asList(names);
        Collections.sort(namesList);
        return namesList;
    }

}
