package cz.dix.pe.problems;

import cz.dix.pe.tools.AdvancedMath;

import java.util.HashMap;
import java.util.Map;

/**
 * Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
 * If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and
 * each of a and b are called amicable numbers.
 * <p/>
 * For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110;
 * therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
 * <p/>
 * Evaluate the sum of all the amicable numbers under 10000.
 */
public class Problem21 implements Problem {

    @Override
    public Object solve() {
        int limit = 10000;

        Map<Integer, Integer> dNCache = new HashMap<Integer, Integer>();
        long finalSum = 0;
        for (int number = 1; number < limit; number++) {
            Integer dN = dNCache.get(number);
            if (dN == null) {
                long[] divisors = AdvancedMath.getDivisors(number);
                dN = (int) AdvancedMath.sum(divisors);
                // sum of divisors can be improved according to
                // http://mathschallenge.net/index.php?section=faq&ref=number/sum_of_divisors
                dNCache.put(number, dN);
            }

            Integer dN2 = dNCache.get(dN);
            if (dN2 == null) {
                long[] divisors = AdvancedMath.getDivisors(dN);
                dN2 = (int) AdvancedMath.sum(divisors);
                dNCache.put(dN, dN2);
            }

            if (number == dN2 && number != dN) { // is amicable number
                finalSum += number;
            }
        }
        return finalSum; // 31626
    }

}
