package cz.dix.pe.problems;

import cz.dix.pe.tools.AdvancedMath;

import java.util.ArrayList;
import java.util.List;

/**
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 * <p/>
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 */
public class Problem5 implements Problem {

    @Override
    public Object solve() {
        int limit = 20;

        List<Long> factorsList = new ArrayList<Long>();
        for (int i = 2; i <= limit; i++) {

            List<Long> factorsListCopy = new ArrayList<Long>(factorsList);
            List<Long> missingFactors = new ArrayList<Long>();
            for (long factor : AdvancedMath.factorize(i)) {
                if (factorsListCopy.contains(factor)) {
                    factorsListCopy.remove(factor);
                } else {
                    missingFactors.add(factor);
                }
            }
            factorsList.addAll(missingFactors);

        }

        long result = 1;
        for (Long i : factorsList) {
            result *= i;
        }
        return result;
    }
}
