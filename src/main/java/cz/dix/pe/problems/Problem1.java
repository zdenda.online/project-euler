package cz.dix.pe.problems;

import java.util.ArrayList;
import java.util.Collection;

/**
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
 * The sum of these multiples is 23.
 * <p/>
 * Find the sum of all the multiples of 3 or 5 below 1000.
 */
public class Problem1 implements Problem {

    @Override
    public Object solve() {
        int limit = 1000;

        Collection<Integer> multiplies = new ArrayList<Integer>();
        for (int i = 1; i < limit; i++) {
            if (i % 3 == 0 || i % 5 == 0) {
                multiplies.add(i);
            }
        }
        long sum = 0;
        for (Integer m : multiplies) {
            sum += m;
        }
        return sum;
    }
}
