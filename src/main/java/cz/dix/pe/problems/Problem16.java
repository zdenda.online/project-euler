package cz.dix.pe.problems;

import cz.dix.pe.tools.AdvancedMath;

/**
 * 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
 * <p/>
 * What is the sum of the digits of the number 2^1000?
 */
public class Problem16 implements Problem {

    @Override
    public Object solve() {
        int exp = 1000;

        String result = "1";
        for (int i = 0; i < exp; i++) {
            result = AdvancedMath.addLongNumbers(result, result); // every power^2 is same as adding two same numbers
        }

        long sum = 0;
        for (int i = 0; i < result.length(); i++) {
            sum += Character.getNumericValue(result.charAt(i));
        }
        return sum;
    }
}
