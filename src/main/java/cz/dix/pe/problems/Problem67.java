package cz.dix.pe.problems;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.
 * <p/>
 * 3
 * 7 4
 * 2 4 6
 * 8 5 9 3
 * <p/>
 * That is, 3 + 7 + 4 + 9 = 23.
 * <p/>
 * Find the maximum total from top to bottom in triangle.txt (right click and 'Save Link/Target As...'), a 15K text file containing a triangle with one-hundred rows.
 */
public class Problem67 implements Problem {

    @Override
    public Object solve() {
        String inputFilePath = "src/main/resources/triangle.txt";

        int[][] triangle = loadTriangle(inputFilePath);
        // bottom to top
        for (int rowIdx = triangle.length - 2; rowIdx >= 0; rowIdx--) {
            for (int colIdx = 0; colIdx < triangle[rowIdx].length; colIdx++) {
                triangle[rowIdx][colIdx] += Math.max(triangle[rowIdx + 1][colIdx], triangle[rowIdx + 1][colIdx + 1]);
            }
        }
        return triangle[0][0];
    }

    private int[][] loadTriangle(String filePath) {
        String contents;
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(filePath));
            contents = Charset.forName("UTF-8").decode(ByteBuffer.wrap(encoded)).toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String[] lines = contents.split("\n");
        int[][] triangle = new int[lines.length][];
        for (int i = 0; i < triangle.length; i++) {
            String line = lines[i];
            String[] row = line.split(" ");
            triangle[i] = new int[row.length];
            for (int j = 0; j < row.length; j++) {
                triangle[i][j] = Integer.valueOf(row[j]);
            }
        }
        return triangle;
    }
}
