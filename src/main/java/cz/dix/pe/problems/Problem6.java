package cz.dix.pe.problems;

/**
 * The sum of the squares of the first ten natural numbers is,
 * <p/>
 * 1^2 + 2^2 + ... + 10^2 = 385
 * The square of the sum of the first ten natural numbers is,
 * <p/>
 * (1 + 2 + ... + 10)2 = 552 = 3025
 * Hence the difference between the sum of the squares of the first ten natural numbers and
 * the square of the sum is 3025 − 385 = 2640.
 * <p/>
 * Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
 */
public class Problem6 implements Problem {

    @Override
    public Object solve() {
        int limit = 100;

        /**
         * sqs = (sum(n))^2 = (n*(n+1)/2)^2
         * ssq = sum(n^2) = n*(n+1)*(2n+1)/6
         * sqs - ssq = n*(n+1)*(3n^2-n-2)/12
         */
        return limit * (limit + 1) * ((3 * limit * limit) - limit - 2) / 12;
    }


}
