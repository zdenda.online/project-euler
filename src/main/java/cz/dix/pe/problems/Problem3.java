package cz.dix.pe.problems;

import cz.dix.pe.tools.AdvancedMath;

/**
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * <p/>
 * What is the largest prime factor of the number 600851475143 ?
 */
public class Problem3 implements Problem {

    @Override
    public Object solve() {
        long testedNum = 600851475143L;

        long[] factors = AdvancedMath.factorize(testedNum);
        long max = Long.MIN_VALUE;
        for (long factor : factors) {
            if (factor > max) {
                max = factor;
            }
        }
        return max;
    }
}
