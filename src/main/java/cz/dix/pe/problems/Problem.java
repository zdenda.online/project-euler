package cz.dix.pe.problems;

public interface Problem {

    Object solve();
}
