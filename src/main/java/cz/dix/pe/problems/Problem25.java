package cz.dix.pe.problems;

import cz.dix.pe.tools.AdvancedMath;

/**
 * The Fibonacci sequence is defined by the recurrence relation:
 * <p/>
 * Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
 * Hence the first 12 terms will be:
 * <p/>
 * F1 = 1
 * F2 = 1
 * F3 = 2
 * F4 = 3
 * F5 = 5
 * F6 = 8
 * F7 = 13
 * F8 = 21
 * F9 = 34
 * F10 = 55
 * F11 = 89
 * F12 = 144
 * The 12th term, F12, is the first term to contain three digits.
 * <p/>
 * What is the first term in the Fibonacci sequence to contain 1000 digits?
 */
public class Problem25 implements Problem {
    @Override
    public Object solve() {
        int digitsCount = 1000;

        String prelast = "1";
        String last = "2";
        int termCounter = 3; // we are starting at 3rd number
        while (last.length() < digitsCount) {
            String lastSave = last;
            last = AdvancedMath.addLongNumbers(prelast, last);
            prelast = lastSave;
            termCounter++;
        }
        return termCounter;
    }
}
