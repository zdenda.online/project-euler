package cz.dix.pe.problems;

import cz.dix.pe.tools.AdvancedMath;

/**
 * n! means n × (n − 1) × ... × 3 × 2 × 1
 * <p/>
 * For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
 * and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
 * <p/>
 * Find the sum of the digits in the number 100!
 */
public class Problem20 implements Problem {

    @Override
    public Object solve() {
        int factorialBase = 100;

        int num = 1;
        String result = "1";
        while (num <= factorialBase) {
            result = AdvancedMath.multiplyLongNumber(result, num);
            num++;
        }

        int sum = 0;
        for (int i = 0; i < result.length(); i++) {
            sum += Character.getNumericValue(result.charAt(i));
        }
        return sum;
    }
}
