package cz.dix.pe.problems;

/**
 * If the numbers 1 to 5 are written out in words: one, two, three, four, five,
 * then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
 * <p/>
 * If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words,
 * how many letters would be used?
 * <p/>
 * <p/>
 * NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters
 * and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers
 * is in compliance with British usage.
 */
public class Problem17 implements Problem {

    @Override
    public Object solve() {
        int limit = 1000;

        int sum = 0;
        for (int i = 1; i < limit + 1; i++) {
            sum += getCharactersCount(i);
        }
        return sum;
    }

    private int getCharactersCount(int number) {
        if (number == 0) {
            return 0;
        } else if (number < 20) {
            switch (number) {
                case 1:
                    return "one".length();
                case 2:
                    return "two".length();
                case 3:
                    return "three".length();
                case 4:
                    return "four".length();
                case 5:
                    return "five".length();
                case 6:
                    return "six".length();
                case 7:
                    return "seven".length();
                case 8:
                    return "eight".length();
                case 9:
                    return "nine".length();
                case 10:
                    return "ten".length();
                case 11:
                    return "eleven".length();
                case 12:
                    return "twelve".length();
                case 13:
                    return "thirteen".length();
                case 14:
                    return "fourteen".length();
                case 15:
                    return "fifteen".length();
                case 16:
                    return "sixteen".length();
                case 17:
                    return "seventeen".length();
                case 18:
                    return "eighteen".length();
                case 19:
                    return "nineteen".length();
            }
        } else if ((number >= 20 && number < 40) ||
                (number >= 80 && number < 100)) { // twenty, thirty, eighty, ninety
            int withoutLastDigit = (number / 10) * 10;
            return "twenty".length() + getCharactersCount(number % withoutLastDigit);
        } else if (number >= 40 && number < 70) { // forty, fifty, sixty
            int withoutLastDigit = (number / 10) * 10;
            return "forty".length() + getCharactersCount(number % withoutLastDigit);
        } else if (number >= 70 && number < 80) {
            return "seventy".length() + getCharactersCount(number % 70);
        } else if (number >= 100 && number < 1000) {
            String hundreds = (number % 100 == 0) ? "hundred" : "hundredand";
            return getCharactersCount(number / 100) + hundreds.length() + getCharactersCount(number % 100);
        } else if (number >= 1000 && number < 1000000) {
            return getCharactersCount(number / 1000) + "thousand".length() + getCharactersCount(number % 1000);
        }
        throw new RuntimeException("Unknown number " + number);
    }
}
