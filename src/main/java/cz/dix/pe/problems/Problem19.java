package cz.dix.pe.problems;

import cz.dix.pe.tools.CalendarUtil;

/**
 * You are given the following information, but you may prefer to do some research for yourself.
 * <p/>
 * 1 Jan 1900 was a Monday.
 * Thirty days has September,
 * April, June and November.
 * All the rest have thirty-one,
 * Saving February alone,
 * Which has twenty-eight, rain or shine.
 * And on leap years, twenty-nine.
 * A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
 * How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
 */
public class Problem19 implements Problem {

    @Override
    public Object solve() {
        int startingYear = 1901;
        int endingYear = 2000;
        int dayToFind = 6; // 6 == Sunday

        int daysSum = 0;
        int firstDay = 1; // 1 Jan 1901 == Tuesday
        for (int year = startingYear; year <= endingYear; year++) {
            for (int month = 1; month <= 12; month++) {
                firstDay = (firstDay + CalendarUtil.getDaysOfMonth(month, year)) % 7;
                if (firstDay == dayToFind) {
                    daysSum++;
                }
            }
        }
        // 2013 => 2
        return daysSum;
    }
}
