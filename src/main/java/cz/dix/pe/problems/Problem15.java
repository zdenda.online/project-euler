package cz.dix.pe.problems;

import java.util.HashMap;
import java.util.Map;

/**
 * Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down,
 * there are exactly 6 routes to the bottom right corner.
 * <p/>
 * {right,right,down,down}, {down,down,right,right}, {right,down,down,right}, {down,right,right,down},
 * {right,down,right,down}, {down,right,down,right}
 * <p/>
 * How many such routes are there through a 20×20 grid?
 */
public class Problem15 implements Problem {

    Map<NumbersPair, Long> resultsCache = new HashMap<NumbersPair, Long>();

    @Override
    public Object solve() {
        // Pascal triangle
        // http://en.wikipedia.org/wiki/Pascal's_triangle
        long result = 1;
        for (int i = 21; i < 41; i++) {
            result = result * i / (i - 20);
        }
        return result;
    }

    /**
     * This method may be used for brute force which is very very slow.
     *
     * @param rightLeft how many right moves left
     * @param downLeft  how many down moves left
     * @return permutation
     */
    private long generatePermutation(int rightLeft, int downLeft) {
        if (rightLeft == 0 || downLeft == 0) {
            return 1; // rest goes left or down
        } else if (rightLeft == 1 || downLeft == 1) {
            return (rightLeft == 1) ? downLeft + 1 : rightLeft + 1;
        } else if (rightLeft == 2 || downLeft == 2) {
            int num = (rightLeft == 2) ? downLeft + 1 : rightLeft + 1;
            return (num * (num + 1)) / 2;
        } else {
            NumbersPair key1 = new NumbersPair(rightLeft - 1, downLeft);
            Long res1 = resultsCache.get(key1);
            if (res1 == null) {
                res1 = generatePermutation(rightLeft - 1, downLeft);
                resultsCache.put(key1, res1);
            }
            NumbersPair key2 = new NumbersPair(rightLeft, downLeft - 1);
            Long res2 = resultsCache.get(key2);
            if (res2 == null) {
                res2 = generatePermutation(rightLeft, downLeft - 1);
                resultsCache.put(key2, res2);
            }
            // 137846528820 in 1216273ms
            return generatePermutation(rightLeft - 1, downLeft) + generatePermutation(rightLeft, downLeft - 1);
        }
    }

    private static class NumbersPair {
        int num1;
        int num2;

        private NumbersPair(int num1, int num2) {
            this.num1 = num1;
            this.num2 = num2;
        }

        @Override
        public int hashCode() {
            if (num1 == 0 || num2 == 0) {
                return 0; // all 0 results have same value
            }
            if (num1 > num2) { // {2,3} == {3,2}
                int swap = num1;
                num2 = num1;
                num1 = swap;
            }
            return (1000 * num1) + num2; // we expect little numbers
        }

        @Override
        public boolean equals(Object obj) {
            NumbersPair other = (NumbersPair) obj;
            return (hashCode() == other.hashCode()); // no need for deeper checking
        }
    }
}

