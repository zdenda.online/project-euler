package cz.dix.pe.problems;

import cz.dix.pe.tools.StringUtil;

/**
 * A palindromic number reads the same both ways.
 * The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
 * <p/>
 * Find the largest palindrome made from the product of two 3-digit numbers.
 */
public class Problem4 implements Problem {

    @Override
    public Object solve() {
        // brute force
        long largest = -1;
        for (long num = 100; num < 1000; num++) {
            for (long num2 = 100; num2 < 1000; num2++) {
                long result = num * num2;
                if (StringUtil.isPalindrome(String.valueOf(result)) && result > largest) {
                    largest = result;
                }
            }
        }
        return largest;
    }
}
