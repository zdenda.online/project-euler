package cz.dix.pe.problems;

import cz.dix.pe.tools.AdvancedMath;

/**
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
 * <p/>
 * What is the 10 001st prime number?
 */
public class Problem7 implements Problem {

    @Override
    public Object solve() {
        int pos = 10001;

        int testedNumbers = 110000;
        int[] primes = AdvancedMath.primesToNumber(testedNumbers);
        if (primes.length < pos) {
            throw new RuntimeException("Raise count of tested primes, too low value " + testedNumbers);
        } else {
            return primes[pos];
        }
    }
}
