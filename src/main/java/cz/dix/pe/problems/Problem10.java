package cz.dix.pe.problems;

import cz.dix.pe.tools.AdvancedMath;

/**
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * <p/>
 * Find the sum of all the primes below two million.
 */
public class Problem10 implements Problem {

    @Override
    public Object solve() {
        int limit = 2000000;

        int[] primes = AdvancedMath.primesToNumber(limit);
        return AdvancedMath.sum(primes);
    }
}
