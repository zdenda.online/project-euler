package cz.dix.pe.problems;

/**
 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
 * <p/>
 * a^2 + b^2 = c^2
 * For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
 * <p/>
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 * Find the product abc.
 */
public class Problem9 implements Problem {

    @Override
    public Object solve() {
        int sumResult = 1000;

        for (int a = 1; a < sumResult; a++) {
            for (int b = a; b < (sumResult - a); b++) {
                int c = sumResult - a - b;
                if ((a * a) + (b * b) == (c * c)) {
                    return a * b * c;
                }
            }
        }
        throw new RuntimeException("No such product exists");
    }
}
