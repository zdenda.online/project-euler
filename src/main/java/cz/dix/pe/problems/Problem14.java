package cz.dix.pe.problems;

import java.util.Arrays;

/**
 * The following iterative sequence is defined for the set of positive integers:
 * <p/>
 * n → n/2 (n is even)
 * n → 3n + 1 (n is odd)
 * <p/>
 * Using the rule above and starting with 13, we generate the following sequence:
 * <p/>
 * 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
 * It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms.
 * Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
 * <p/>
 * Which starting number, under one million, produces the longest chain?
 * <p/>
 * NOTE: Once the chain starts the terms are allowed to go above one million.
 */
public class Problem14 implements Problem {

    @Override
    public Object solve() {
        int limit = 1000000;

        boolean[] foundNumbers = new boolean[limit];
        Arrays.fill(foundNumbers, false);
        foundNumbers[0] = true;

        int testedNumber;
        int numberWithLongestChain = -1;
        int longestChainLength = -1;
        long actualNumber = limit - 1;
        do {
            testedNumber = (int) actualNumber;

            int chainLength = 1; // +1 for starting at 1
            while (actualNumber != 1) {
                if (actualNumber < foundNumbers.length) {
                    foundNumbers[((int) actualNumber) - 1] = true;
                }
                if (actualNumber % 2 == 0) {
                    actualNumber = actualNumber / 2;
                } else {
                    actualNumber = (actualNumber * 3) + 1;
                }
                chainLength++;
            }

            if (chainLength > longestChainLength) {
                longestChainLength = chainLength;
                numberWithLongestChain = testedNumber;
            }

            // find next number to be tested
            for (int numCandidate = testedNumber - 1; numCandidate >= 0; numCandidate--) {
                if (!foundNumbers[numCandidate]) {
                    actualNumber = numCandidate + 1;
                    break;
                }
            }

        } while (actualNumber != 1); // no new candidate found

        return numberWithLongestChain;
    }
}
