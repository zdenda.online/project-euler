package cz.dix.pe;

import cz.dix.pe.problems.Problem;
import cz.dix.pe.problems.Problem67;

public class Main {

    public static void main(String[] args) {
        Problem problem = new Problem67();

        long start = System.currentTimeMillis();
        Object result = problem.solve();
        long end = System.currentTimeMillis();
        System.out.print("Result is: " + result + " in " + (end - start) + "ms");
    }
}
