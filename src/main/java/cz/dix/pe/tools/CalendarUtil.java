package cz.dix.pe.tools;

/**
 * @author Zdenek Obst
 */
public class CalendarUtil {

    /**
     * Gets number of days in month of given year.
     *
     * @param month month
     * @param year  year
     * @return number of days (30/31/28/29) of given month
     */
    public static int getDaysOfMonth(int month, int year) {
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            return 31;
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            return 30;
        } else {
            if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) {
                return 29;
            } else {
                return 28;
            }
        }

    }
}
