package cz.dix.pe.tools;

/**
 *
 */
public class StringUtil {

    /**
     * Checks whether passed string palindrome.
     * Palindrome is a string that is same if read from the beginning and the end.
     * E.g. "rats live on no evil star" is a palindrome
     *
     * @param text text to be checked
     * @return true if given text is the palindrome, otherwise false
     */
    public static boolean isPalindrome(String text) {
        text = text.toLowerCase();
        for (int i = 0; i < (text.length() / 2); i++) {
            if (text.charAt(i) != text.charAt(text.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Reverses given source string.
     * E.g. HOTEL -> LETOH
     *
     * @param src source string to be reversed
     * @return reversed string
     */
    public static String reverseString(String src) {
        return new StringBuilder(src).reverse().toString();
    }

    /**
     * Gets an alphabetical value of given string. The alphabetical value is sum of its characters.
     * E.g.: abc = 1 + 2 + 3 = 6
     *
     * @param str string for which value should be counted
     * @return alphabetical value of string
     */
    public static int getAlphabeticalValue(String str) {
        str = str.toUpperCase();
        int value = 0;
        for (int i = 0; i < str.length(); i++) {
            value += (str.charAt(i) - 64);
        }
        return value;
    }

}
