package cz.dix.pe.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class AdvancedMath {

    /**
     * Gets all primes lower than given number.
     *
     * @param limitNumber limit number
     * @return all primes that are smaller than given number
     */
    public static int[] primesToNumber(int limitNumber) {
        boolean[] primes = new boolean[limitNumber];
        Arrays.fill(primes, true);
        primes[0] = primes[1] = false; // 0, 1 not primes
        for (int i = 2; i < primes.length; i++) {
            if (primes[i]) {
                for (int j = 2; i * j < primes.length; j++) {
                    primes[i * j] = false; // invalidate all future numbers
                }
            }
        }

        Collection<Integer> primeNumbers = new ArrayList<Integer>();
        for (int i = 0; i < primes.length; i++) {
            if (primes[i]) {
                primeNumbers.add(i);
            }
        }

        return convertIntArray(primeNumbers.toArray(new Integer[primeNumbers.size()]));
    }

    /**
     * Counts a sum of given numbers.
     *
     * @param numbers numbers for the sum
     * @return sum of given numbers
     */
    public static long sum(long[] numbers) {
        long sum = 0;
        for (long number : numbers) {
            sum += number;
        }
        return sum;
    }

    /**
     * Counts a sum of given numbers.
     *
     * @param numbers numbers for the sum
     * @return sum of given numbers
     */
    public static long sum(int[] numbers) {
        long sum = 0;
        for (long number : numbers) {
            sum += number;
        }
        return sum;
    }

    /**
     * Factorizes given number to its factors (primes).
     *
     * @param number number to be factorized
     * @return factors of given number
     */
    public static long[] factorize(long number) {
        long actualNum = number;
        Collection<Long> factors = new ArrayList<Long>();
        for (long candidate = 2; candidate <= actualNum / candidate; candidate++) {
            while (actualNum % candidate == 0) {
                factors.add(candidate);
                actualNum /= candidate;
            }
        }
        if (actualNum > 1) {
            factors.add(actualNum);
        }

        return convertLongArray(factors.toArray(new Long[factors.size()]));
    }

    /**
     * Gets divisors of given number. Divisor is a number that satisfies formula "number % divisor == 0".
     *
     * @param number number for divisors
     * @return divisors of given number
     */
    public static long[] getDivisors(long number) {
        Collection<Long> divisors = new ArrayList<Long>();
        divisors.add(1L);
        for (long candidate = 2; candidate <= number / 2; candidate++) {
            if (number % candidate == 0) {
                divisors.add(candidate);
            }
        }
        return convertLongArray(divisors.toArray(new Long[divisors.size()]));
    }

    /**
     * Adds a long numbers that cannot be represented by primitive types and thus are passed as strings.
     * Uses a method that makes sum of least significant digits in number and using its last number as the result
     * while the rest is used as a transfer to next sum or more significant digit.
     *
     * @param numbers numbers to be added
     * @return result of addition
     */
    public static String addLongNumbers(String... numbers) {
        String[] inverseNumbers = new String[numbers.length];
        int i = 0;
        for (String number : numbers) {
            inverseNumbers[i++] = StringUtil.reverseString(number);
        }

        int digitIdx = 0;
        int transfer = 0;
        StringBuilder resultBuilder = new StringBuilder();
        while (true) {
            int result = 0;
            boolean anythingAdded = false;
            for (String number : inverseNumbers) {
                if (number.length() > digitIdx) {
                    anythingAdded = true;
                    result += Character.getNumericValue(number.charAt(digitIdx));
                }
            }
            if (anythingAdded) {
                result += transfer;
                transfer = result / 10;
                resultBuilder.append(result % 10);
                digitIdx++;
            } else {
                while (transfer != 0) {
                    resultBuilder.append(transfer % 10);
                    transfer /= 10;
                }
                break; // no values to add
            }
        }
        return StringUtil.reverseString(resultBuilder.toString());
    }

    /**
     * Multiples a long number by any "short" one. A long number is represented as string while it cannot be represented
     * by any of primitive types.
     *
     * @param longNumber long number
     * @param multiplier short number
     * @return result of multiplication
     */
    public static String multiplyLongNumber(String longNumber, long multiplier) {
        String[] partialResults = new String[longNumber.length()];
        for (int i = 0; i < longNumber.length(); i++) {
            long res = Character.getNumericValue(longNumber.charAt(i)) * multiplier;
            StringBuilder partialResBuilder = new StringBuilder(String.valueOf(res));
            for (int j = 0; j < (longNumber.length() - 1 - i); j++) {
                partialResBuilder.append("0");
            }
            partialResults[i] = partialResBuilder.toString();
        }
        return addLongNumbers(partialResults);
    }

    private static int[] convertIntArray(Integer[] array) {
        int[] asPrimitive = new int[array.length];
        int i = 0;
        for (Integer num : array) {
            asPrimitive[i++] = num;
        }
        return asPrimitive;
    }

    private static long[] convertLongArray(Long[] array) {
        long[] asPrimitive = new long[array.length];
        int i = 0;
        for (Long num : array) {
            asPrimitive[i++] = num;
        }
        return asPrimitive;
    }
}
